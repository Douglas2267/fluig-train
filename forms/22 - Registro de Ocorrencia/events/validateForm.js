function validateForm(form){
	var nrAtividade = getValue("WKNumState");

	if (nrAtividade !== null || nrAtividade !== undefined || nrAtividade !== '') {
		//Variáveis
		var msg = '';
		var nmArea =                  form.getValue('nmArea')
		var nmOrigem=				  form.getValue('nmOrigem')
		var fgOcorrenciaReincidente = form.getValue('fgOcorrenciaReincidente')
		var cdOcorrenciaReincidente = form.getValue('cdOcorrenciaReincidente')
		var dsOcorrencia = form.getValue('dsOcorrencia')
		//Validações
		if(nmArea == null || nmArea == undefined){
			msg+= "O campo area deve ser preenchido\n";
		}
		if(nmOrigem == null || nmOrigem == undefined){
			msg+= "O campo origem deve ser preenchido\n";
		}
		//Se tiver selecionado o radio , ele valida
		if(fgOcorrenciaReincidente == "Yes" || fgOcorrenciaReincidente == ""){
			console.log('object', fgOcorrenciaReincidente);
			msg+= "Como foi informado que esta é uma ocorrência reincidente, seu número deve ser preenchido\n";
			if(cdOcorrenciaReincidente == null || cdOcorrenciaReincidente == undefined || cdOcorrenciaReincidente == ''){
				msg+= "Número da ocorrência deve ser preenchido\n";
			}
		}
		
		if(dsOcorrencia == null || dsOcorrencia == undefined || dsOcorrencia == ''){
			msg+= "O campo descrição da ocorrência deve ser preenchido\n";
		}
	}

	
	if(msg != ""){
		throw msg;
	}


}