function displayFields(form,customHTML){ 
	log.info("====================== DISPLAY FIELDS ======================");
		form.setShowDisabledFields(false);
		form.setHidePrintLink(true);
		var numActivity = getValue("WKNumState");
	
		//Atividades
		var INICIO_SOLICITACAO = 0;
		var ATIVIDADE_INICIAL = 1;
		var APROVAR_SOLICITACAO = 2;
		var EXECUTAR_CONTENCAO = 4;
		var ANALISAR_CAUSA = 5;
		var APROVAR_ANALISE = 6;
		var REGISTRAR_PLANO = 7;
		var VERIFICAR_EFICACIA = 9;
		var NOTIFICAR = 10;
		var DEFINIR_PRAZO = 18;
	
		//Conjunto de campos 
		var data = '';
		var dadosDetalhes =[];
		var dados1 = ["dsOrigem","fgContencao","dsOcorrencia","nmTipoAcao","nmOcorrencia","nmCriticidade","nmAssunto"];
		var dados2 = ["dsOrigem", "fgContencao", "fgOcorrenciaReincidente", "cdOcorrenciaReincidente","dsAnaliseCausa","dsMedicaoPrincipal","dsMedicaoSecundaria","dsMaodeObraPrincipal",	"dsMaodeObraSecundaria","dsMaterialPrincipal","dsMaterialSecundario","dsMetodoPrincipal","dsMetodoSecundario","dsMeioAmbientePrincipal","dsMeioAmbienteSecundario","dsMaquinaPrincipal","dsMaquinaSecundario","dsEfeito","dsAcoesExecutadas","dsEficacia","nmTipoAcao","nmTipoOcorrencia","nmArea","nmOrigem","nmCriticidade","nmAssunto","dtOcorrencia","cincopq_pergunta_1","cincopq_pergunta_2","cincopq_pergunta_3","cincopq_pergunta_4","cincopq_pergunta_5","cincopq_resposta_1","cincopq_resposta_2","cincopq_resposta_3","cincopq_resposta_4","cincopq_resposta_5","cincopq_causa_raiz"];
		var dados3 = ["cincopq_pergunta_1","cincopq_pergunta_2","cincopq_pergunta_3","cincopq_pergunta_4","cincopq_pergunta_5","cincopq_resposta_1","cincopq_resposta_2","cincopq_resposta_3","cincopq_resposta_4","cincopq_resposta_5","cincopq_causa_raiz"];
		var dados4 = ["dsOrigem","fgContencao","dsOcorrencia","dsContencao","fgOcorrenciaReincidente","cdOcorrenciaReincidente"];
		var dados5 = ["dsAnaliseCausa",	"dsMedicaoPrincipal","dsMedicaoSecundaria",	"dsMaodeObraPrincipal",	"dsMaodeObraSecundaria","dsMaterialPrincipal","dsMaterialSecundario","dsMetodoPrincipal","dsMetodoSecundario","dsMeioAmbientePrincipal","dsMeioAmbienteSecundario","dsMaquinaPrincipal","dsMaquinaSecundario","dsEfeito"];		
		var dados6 = ["dsAcoesExecutadas","dsEficacia","nmTipoAcao","nmTipoOcorrencia","nmArea","nmOrigem","nmCriticidade",	"nmAssunto","dtOcorrencia"];
		//Abrindo Jquery Custom
		customHTML.append("<script>");
		customHTML.append("$(document).ready(function(){");
	
		//Atividades de Aprovador
		if(numActivity == APROVAR_SOLICITACAO || numActivity == APROVAR_ANALISE || numActivity == VERIFICAR_EFICACIA) {
			esconderDIV("#divContencao");
			mostrarDIV("#divAnalise");
			mostrarDIV("#divDescricaoAcoes");
			mostrarDIV("#divVerificacao");		
			//Funcão para habilitar todos os campos do form
			(function iterarCard() {
				var habilitar = true; // Informe True para Habilitar ou False para Desabilitar os campos
				var mapaForm = new java.util.HashMap();
				mapaForm = form.getCardData();
				var it = mapaForm.keySet().iterator();
			 
				while (it.hasNext()) { // Laço de repetição para habilitar/desabilitar os campos
					var key = it.next();
					form.setEnabled(key, habilitar);
			}
			})();
		}
		//Atividade Inicial ou Iniciar Solicitação
		if(numActivity == ATIVIDADE_INICIAL || numActivity == INICIO_SOLICITACAO) {
			var c1 = DatasetFactory.createConstraint("colleaguePK.colleagueId", getValue("WKUser"), getValue("WKUser"), ConstraintType.MUST);
			var constraints = new Array(c1);
			var colaborador = DatasetFactory.getDataset("colleague", null, constraints, null);
			
			customHTML.append("$('#_dsOcorrencia').attr('readonly', false);");			
			form.setValue('status', numActivity);
			form.setValue('nmUsuario', colaborador.getValue(0, "colleagueName"));
			form.setValue('matriculaUsuario', colaborador.getValue(0, "colleaguePK.colleagueId"));
			habilitarCampos(dados1);
			esconderDIV("#divContencao");
	//função auxiliar para mapear a data do dia
			(function date(params) {
				//Pega data atual formatada
				var fullDate = new Date();
				var hours = fullDate.getHours();
				var minutes = fullDate.getMinutes();
				if (minutes <= 9){ minutes = "0" + minutes; }
				var timeValue = hours + ":" + minutes;
				var date = fullDate.getDate().toString();
				if(date.length == 1){ date = 0+date; }
				var mes = (fullDate.getMonth()+1).toString();	
				if(mes.length == 1){ mes = 0+mes; }
				var data = date+"/"+mes+"/"+fullDate.getFullYear();
			
	
			//seta data atual
			form.setValue('dtRegistro',data);
			form.setValue('dtOcorrencia',data);
			})();	
		}

		else if (numActivity == APROVAR_SOLICITACAO) {
			habilitarCampos(dados2);
		}
		else if (numActivity == EXECUTAR_CONTENCAO){
			mostrarDIV("#divContencao");
			esconderDIV("#divVerificacao");
			esconderDIV("#divDescricaoAcoes");
			esconderDIV("#divAnalise");
			desabilitarCampos(dados2);
		}	
		else if (numActivity == ANALISAR_CAUSA){
			esconderDIV("#divDetalhes");
			form.setVisible('dsContencao',true);
			desabilitarCampos(dados4)
			desabilitarCampos(dados6)
			habilitarCampos(dados5);
			esconderDIV("#divVerificacao");
			esconderDIV("#linhaContencao");	
		}
	
		else if(numActivity == APROVAR_ANALISE){
			esconderDIV("#linhaContencao");
			esconderDIV("#divVerificacao");
			mostrarDIV("#divContencao");
			mostrarDIV("#divAnalise");
			mostrarDIV("#divDescricaoAcoes");

			(function iterarCard2() {
				var habilitar = true; // Informe True para Habilitar ou False para Desabilitar os campos
					var mapaForm = new java.util.HashMap();
					mapaForm = form.getCardData();
					var it = mapaForm.keySet().iterator();
					
					while (it.hasNext()) { // Laço de repetição para habilitar/desabilitar os campos
						var key = it.next();
						form.setEnabled(key, habilitar);
					}
			})();
			habilitarCampos(dados3);
			}
	
		else if(numActivity == REGISTRAR_PLANO){
			desabilitarCampos(dados1,dados2,dados3);	
			form.setValue('envio', 'passaValidate');	
			}
	
		else if(numActivity == VERIFICAR_EFICACIA){
			desabilitarCampos(dados1,dados2,dados3);
			form.setEnabled("dsEficacia", true);
			}
	
		else if (numActivity == NOTIFICAR) {
			desabilitarCampos(dados1,dados2,dados3);	
		}
	
		else {
			desabilitarCampos(dados1,dados2,dados3);	
		}
		
		form.setEnabled("cdOcorrencia", false);
		form.setEnabled("nmUsuario", false);
		form.setEnabled("dtRegistro", false);
		log.info("right before customHTML")
		
		customHTML.append("});");
		customHTML.append("</script>");
	
		customHTML.append("<script>function getWKNumState(){ return " + getValue("WKNumState") + "; }</script>");
	
	
		if(numActivity == DEFINIR_PRAZO) {
			esconderDIV("#divContencao");
			esconderDIV("#divDetalhes");
			mostrarDIV("#divSolicitacao");
			mostrarDIV("#divDtEficacia");
			form.setEnabled("dtEficacia", true);
		}
	
	///Funções Auxiliares
		function desabilitarCampos(campos) {
			campos.forEach(function(campo) {
				form.setEnabled(campo, false);
			});
		}
		function habilitarCampos(campos){
			campos.forEach(function(campo){ 
				form.setEnabled(campo, true); 
			});		
		}
		function esconderDIV(ocultadiv) {
			customHTML.append("$('" + ocultadiv + "').hide();");
		}
		function mostrarDIV(mostradiv) {
			customHTML.append("$('" + mostradiv + "').show();");
		}
	}